package gulajava.teslibraryrests.internet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class CekInternet {

    private Context konteks = null;
    private NetworkInfo netinfo = null;
    private ConnectivityManager conmanager = null;
    private boolean isInternet = false;

    public CekInternet(Context konteks) {
        this.konteks = konteks;

        conmanager = (ConnectivityManager) konteks.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    //  CEK APAKAH STATUS INTERNET MENYALA ATAU TIDAK
    public boolean cekStatusInternet() {

        netinfo = conmanager.getActiveNetworkInfo();

        isInternet = netinfo != null && netinfo.isConnected();

        return isInternet;
    }
}
