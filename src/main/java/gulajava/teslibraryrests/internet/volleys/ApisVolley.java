package gulajava.teslibraryrests.internet.volleys;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;

import java.util.Map;

import gulajava.teslibraryrests.internet.KonstanInternet;
import gulajava.teslibraryrests.modelan.KandidatModel;

/**
 * Created by Gulajava Ministudio on 2/2/16.
 */
public class ApisVolley {

    public static int JUMLAH_TIMEOUT = 2500;
    public static int JUMLAH_COBA = 2;
    public static float PENGALI_TIMEOUT = 1;


    private static final String API_CALONPILKADA = "/calonpilkada/api/candidates";


    public static String getLinkCalonPilkada(String limit) {

        return "http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=" + KonstanInternet.APIKEYS + "&limit=" + limit;
    }


    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    public static JacksonRequest<KandidatModel> getKandidatRequest(
            int methods,
            String urls,
            Map<String, String> headers,
            Map<String, String> params,
            String jsonbodystr,
            Response.Listener<KandidatModel> listenerok,
            Response.ErrorListener listenergagal
    ) {

        headers.put(KonstanInternet.TAG_HEADERCONTENTIPE, KonstanInternet.HEADER_JSONTYPE);

        JacksonRequest<KandidatModel> jacksonRequest = new JacksonRequest<>(
                methods,
                urls,
                KandidatModel.class,
                headers,
                params,
                jsonbodystr,
                listenerok,
                listenergagal
        );

        jacksonRequest.setRetryPolicy(getRetryPolicy());

        return jacksonRequest;
    }


    private static DefaultRetryPolicy getRetryPolicy() {

        return new DefaultRetryPolicy(JUMLAH_TIMEOUT, JUMLAH_COBA, PENGALI_TIMEOUT);
    }

}
